package com.xsis.tugasqc.Controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping(value ="/family")
public class FamilyController {
	@GetMapping(value = "index")
    public ModelAndView index()
    {
        ModelAndView view = new ModelAndView("family/index");
        return view;
    }
}

package com.xsis.tugasqc.Controllers;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.xsis.tugasqc.Models.Biodata;
import com.xsis.tugasqc.Repositories.BiodataRepo;


@RestController
@CrossOrigin("*")
@RequestMapping("/api")
public class ApiBiodataController {
	 @Autowired
	    private BiodataRepo biodataRepo;
	 
	 @GetMapping("/biodata")
	    public ResponseEntity<List<Biodata>> GetAllBiodata()
	    {
	        try {
	            List<Biodata> biodata = this.biodataRepo.findAll();
	            return new ResponseEntity<>(biodata, HttpStatus.OK);
	        }

	        catch (Exception exception) {
	            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	        }
	    }
	 
	 @PostMapping("/biodata")
	    public ResponseEntity<Object> SaveBiodata(@RequestBody Biodata biodata)
	    {
	        try {
	        	biodata.setCreatedBy("Admin");
	        	biodata.setCreatedAt(new Date());
	            this.biodataRepo.save(biodata);
	            return new ResponseEntity<>(biodata, HttpStatus.OK);
	        }

	        catch (Exception exception)
	        {
	            return new ResponseEntity<>("Failed", HttpStatus.BAD_REQUEST);
	        }
	    }
	 
	 @GetMapping("/biodata/{id}")
	    public ResponseEntity<List<Biodata>> GetBiodataById(@PathVariable("id") Long id)
	    {
	        try {
	            Optional<Biodata> biodata = this.biodataRepo.findById(id);

	            if (biodata.isPresent())
	            {
	                ResponseEntity rest = new ResponseEntity<>(biodata, HttpStatus.OK);
	                return rest;
	            } else {
	                return ResponseEntity.notFound().build();
	            }
	        }

	        catch (Exception exception) {
	            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	        }
	    }
	 
//	 @GetMapping("/searchbiodata/{keyword}")
//	    public ResponseEntity<List<Biodata>> SearchBiodataName(@PathVariable("keyword") String keyword)
//	    {
//	        if (keyword != null)
//	        {
//	            List<Biodata> biodata = this.biodataRepo.SearchBiodata(keyword);
//	            return new ResponseEntity<>(biodata, HttpStatus.OK);
//	        } else {
//	            List<Biodata> biodata = this.biodataRepo.findAll();
//	            return new ResponseEntity<>(biodata, HttpStatus.OK);
//	        }
//	    }
	 
	 @GetMapping("/biodatamapped")
	    public ResponseEntity<Map<String, Object>> GetAllPage(@RequestParam(defaultValue = "0") int page, @RequestParam(defaultValue = "5") int size)
	    {
	        try {
	            List<Biodata> biodata = new ArrayList<>();
	            Pageable pagingSort = PageRequest.of(page, size);

	            Page<Biodata> pageTuts;

	            pageTuts = biodataRepo.findAll(pagingSort);

	            biodata = pageTuts.getContent();

	            Map<String, Object> response = new HashMap<>();
	            response.put("biodata", biodata);
	            response.put("currentPage", pageTuts.getNumber());
	            response.put("totalItems", pageTuts.getTotalElements());
	            response.put("totalPages", pageTuts.getTotalPages());

	            return new ResponseEntity<>(response, HttpStatus.OK);
	        }

	        catch (Exception e)
	        {
	            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
	        }
	    }
	 
	 @PutMapping("/biodata/{id}")
	    public ResponseEntity<Object> UpdateBiodata(@RequestBody Biodata biodata, @PathVariable("id") Long id)
	    {
	        Optional<Biodata> biodataData = this.biodataRepo.findById(id);

	        if (biodataData.isPresent())
	        {
	        	biodata.setModifiedBy("Admin");
	        	biodata.setModifiedAt(new Date());
	        	biodata.setId(id);
	            this.biodataRepo.save(biodata);
	            ResponseEntity rest = new ResponseEntity<>("Success", HttpStatus.OK);
	            return rest;
	        } else {
	            return ResponseEntity.notFound().build();
	        }
	    }
	 
	 @DeleteMapping("/biodata/{id}")
	    public ResponseEntity<Object> DeleteBiodata(@PathVariable("id") Long id)
	    {
	        this.biodataRepo.deleteById(id);
	        return new ResponseEntity<>("Success", HttpStatus.OK);
	    }
}

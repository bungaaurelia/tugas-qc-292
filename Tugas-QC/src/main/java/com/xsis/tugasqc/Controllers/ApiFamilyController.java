package com.xsis.tugasqc.Controllers;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.xsis.tugasqc.Models.Family;
import com.xsis.tugasqc.Repositories.FamilyRepo;

@RestController
@CrossOrigin("*")
@RequestMapping("/api")
public class ApiFamilyController {
    @Autowired
    private FamilyRepo familyRepo;

    @GetMapping("/family")
    public ResponseEntity<List<Family>> GetAllFamily()
    {
        try {
            List<Family> family = this.familyRepo.findAll();
            return new ResponseEntity<>(family, HttpStatus.OK);
        }

        catch (Exception exception) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @GetMapping("/familybybiodata/{id}")
    public ResponseEntity<List<Family>> GetAllFamilyByBiodataId(@PathVariable("id") Long id)
    {
        try
        {
            List<Family> family = this.familyRepo.FindByBiodataId(id);
            return new ResponseEntity<>(family, HttpStatus.OK);
        }

        catch (Exception exception)
        {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @GetMapping("/family/{id}")
    public ResponseEntity<List<Family>> GetFamilyById(@PathVariable("id") Long id)
    {
        try
        {
            Optional<Family> family = this.familyRepo.findById(id);

            if (family.isPresent())
            {
                ResponseEntity rest = new ResponseEntity<>(family, HttpStatus.OK);
                return rest;
            } else {
                return ResponseEntity.notFound().build();
            }
        }

        catch (Exception exception)
        {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

//    @GetMapping("/searchfamily/{keyword}")
//    public ResponseEntity<List<Family>> SearchFamilyName(@PathVariable("keyword") String keyword)
//    {
//        if (keyword != null)
//        {
//            List<Family> family = this.familyRepo.SearchFamily(keyword);
//            return new ResponseEntity<>(family, HttpStatus.OK);
//        } else {
//            List<Family> family = this.familyRepo.findAll();
//            return new ResponseEntity<>(family, HttpStatus.OK);
//        }
//    }

    @GetMapping("/familymapped")
    public ResponseEntity<Map<String, Object>> GetAllPage(@RequestParam(defaultValue = "0") int page, @RequestParam(defaultValue = "5") int size)
    {
        try {
            List<Family> family = new ArrayList<>();
            Pageable pagingSort = PageRequest.of(page, size);

            Page<Family> pageTuts;

            pageTuts = familyRepo.findAll(pagingSort);

            family = pageTuts.getContent();

            Map<String, Object> response = new HashMap<>();
            response.put("family", family);
            response.put("currentPage", pageTuts.getNumber());
            response.put("totalItems", pageTuts.getTotalElements());
            response.put("totalPages", pageTuts.getTotalPages());

            return new ResponseEntity<>(response, HttpStatus.OK);
        }

        catch (Exception e)
        {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/family")
    public ResponseEntity<Object> SaveFamily(@RequestBody Family family)
    {
        try
        {
        	family.setCreatedBy("Admin");
        	family.setCreatedAt(new Date());
            this.familyRepo.save(family);
            return new ResponseEntity<>(family, HttpStatus.OK);
        }

        catch (Exception exception)
        {
            return new ResponseEntity<>("Failed", HttpStatus.BAD_REQUEST);
        }
    }

    @PutMapping("/family/{id}")
    public ResponseEntity<Object> UpdateFamily(@RequestBody Family family, @PathVariable("id") Long id)
    {
        Optional<Family> familyData = this.familyRepo.findById(id);

        if (familyData.isPresent())
        {
        	family.setModifiedBy("Admin");
        	family.setModifiedAt(new Date());
        	family.setId(id);
            this.familyRepo.save(family);
            ResponseEntity rest = new ResponseEntity<>("Success", HttpStatus.OK);
            return rest;
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @DeleteMapping("/family/{id}")
    public ResponseEntity<Object> DeleteContactPerson(@PathVariable("id") Long id)
    {
        this.familyRepo.deleteById(id);
        return new ResponseEntity<>("Success", HttpStatus.OK);
    }
}



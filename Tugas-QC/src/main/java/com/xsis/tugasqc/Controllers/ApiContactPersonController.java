package com.xsis.tugasqc.Controllers;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.xsis.tugasqc.Models.ContactPerson;
import com.xsis.tugasqc.Repositories.ContactPersonRepo;


@RestController
@CrossOrigin("*")
@RequestMapping("/api")
public class ApiContactPersonController {
    @Autowired
    private ContactPersonRepo contactPersonRepo;

    @GetMapping("/contactperson")
    public ResponseEntity<List<ContactPerson>> GetAllContactPerson()
    {
        try {
            List<ContactPerson> contactPerson = this.contactPersonRepo.findAll();
            return new ResponseEntity<>(contactPerson, HttpStatus.OK);
        }

        catch (Exception exception) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @GetMapping("/contactpersonbybiodata/{id}")
    public ResponseEntity<List<ContactPerson>> GetAllContactPersonByBiodataId(@PathVariable("id") Long id)
    {
        try
        {
            List<ContactPerson> contactPerson = this.contactPersonRepo.FindByBiodataId(id);
            return new ResponseEntity<>(contactPerson, HttpStatus.OK);
        }

        catch (Exception exception)
        {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @GetMapping("/contactperson/{id}")
    public ResponseEntity<List<ContactPerson>> GetContactPersonById(@PathVariable("id") Long id)
    {
        try
        {
            Optional<ContactPerson> contactPerson = this.contactPersonRepo.findById(id);

            if (contactPerson.isPresent())
            {
                ResponseEntity rest = new ResponseEntity<>(contactPerson, HttpStatus.OK);
                return rest;
            } else {
                return ResponseEntity.notFound().build();
            }
        }

        catch (Exception exception)
        {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

//    @GetMapping("/searchcontactperson/{keyword}")
//    public ResponseEntity<List<ContactPerson>> SearchContactPersonName(@PathVariable("keyword") String keyword)
//    {
//        if (keyword != null)
//        {
//            List<ContactPerson> contactPerson = this.contactPersonRepo.SearchContactPerson(keyword);
//            return new ResponseEntity<>(contactPerson, HttpStatus.OK);
//        } else {
//            List<ContactPerson> contactPerson = this.contactPersonRepo.findAll();
//            return new ResponseEntity<>(contactPerson, HttpStatus.OK);
//        }
//    }

    @GetMapping("/contactpersonmapped")
    public ResponseEntity<Map<String, Object>> GetAllPage(@RequestParam(defaultValue = "0") int page, @RequestParam(defaultValue = "5") int size)
    {
        try {
            List<ContactPerson> contactPerson = new ArrayList<>();
            Pageable pagingSort = PageRequest.of(page, size);

            Page<ContactPerson> pageTuts;

            pageTuts = contactPersonRepo.findAll(pagingSort);

            contactPerson = pageTuts.getContent();

            Map<String, Object> response = new HashMap<>();
            response.put("contactPerson", contactPerson);
            response.put("currentPage", pageTuts.getNumber());
            response.put("totalItems", pageTuts.getTotalElements());
            response.put("totalPages", pageTuts.getTotalPages());

            return new ResponseEntity<>(response, HttpStatus.OK);
        }

        catch (Exception e)
        {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/contactPerson")
    public ResponseEntity<Object> SaveContactPerson(@RequestBody ContactPerson contactPerson)
    {
        try
        {
        	contactPerson.setCreatedBy("Admin");
        	contactPerson.setCreatedAt(new Date());
            this.contactPersonRepo.save(contactPerson);
            return new ResponseEntity<>(contactPerson, HttpStatus.OK);
        }

        catch (Exception exception)
        {
            return new ResponseEntity<>("Failed", HttpStatus.BAD_REQUEST);
        }
    }

    @PutMapping("/contactPerson/{id}")
    public ResponseEntity<Object> UpdateContactPerson(@RequestBody ContactPerson contactPerson, @PathVariable("id") Long id)
    {
        Optional<ContactPerson> contactPersonData = this.contactPersonRepo.findById(id);

        if (contactPersonData.isPresent())
        {
        	contactPerson.setModifiedBy("Admin");
        	contactPerson.setModifiedAt(new Date());
        	contactPerson.setId(id);
            this.contactPersonRepo.save(contactPerson);
            ResponseEntity rest = new ResponseEntity<>("Success", HttpStatus.OK);
            return rest;
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @DeleteMapping("/contactPerson/{id}")
    public ResponseEntity<Object> DeleteContactPerson(@PathVariable("id") Long id)
    {
        this.contactPersonRepo.deleteById(id);
        return new ResponseEntity<>("Success", HttpStatus.OK);
    }
}



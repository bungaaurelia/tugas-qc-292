package com.xsis.tugasqc.Models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "biodata")
public class Biodata extends CommonEntity{
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false)
	private long Id;
	
	@Column(name = "first_name", nullable = false)
	private String FirstName;
	
	@Column(name = "last_name")
	private String LastName;
	
	@Column(name = "dob")
	private String Dob;
	
	@Column(name = "pob")
	private String Pob;
	
	@Column(name = "adress")
	private String Adress;
	
	@Column(name = "marital_status")
	private Boolean MaritalStatus;

	public long getId() {
		return Id;
	}

	public void setId(long id) {
		Id = id;
	}

	public String getFirstName() {
		return FirstName;
	}

	public void setFirstName(String firstName) {
		FirstName = firstName;
	}

	public String getLastName() {
		return LastName;
	}

	public void setLastName(String lastName) {
		LastName = lastName;
	}

	public String getDob() {
		return Dob;
	}

	public void setDob(String dob) {
		Dob = dob;
	}

	public String getPob() {
		return Pob;
	}

	public void setPob(String pob) {
		Pob = pob;
	}

	public String getAdress() {
		return Adress;
	}

	public void setAdress(String adress) {
		Adress = adress;
	}

	public Boolean getMaritalStatus() {
		return MaritalStatus;
	}

	public void setMaritalStatus(Boolean maritalStatus) {
		MaritalStatus = maritalStatus;
	}
}

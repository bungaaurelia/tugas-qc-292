package com.xsis.tugasqc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TugasQcApplication {

	public static void main(String[] args) {
		SpringApplication.run(TugasQcApplication.class, args);
	}

}

package com.xsis.tugasqc.Repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.xsis.tugasqc.Models.Biodata;

public interface BiodataRepo extends JpaRepository<Biodata, Long>{

}

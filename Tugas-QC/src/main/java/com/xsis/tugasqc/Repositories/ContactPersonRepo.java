package com.xsis.tugasqc.Repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.xsis.tugasqc.Models.ContactPerson;

public interface ContactPersonRepo extends JpaRepository<ContactPerson, Long>{
	@Query("FROM ContactPerson WHERE BiodataId = ?1")
    List<ContactPerson> FindByBiodataId(Long biodataId);
}

package com.xsis.tugasqc.Repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.xsis.tugasqc.Models.Family;

public interface FamilyRepo extends JpaRepository<Family, Long>{
	@Query("FROM Family WHERE BiodataId = ?1")
    List<Family> FindByBiodataId(Long biodataId);
}
